### NAVIGATION AT ADMIN DASHBOARD

* #### SUMMARY
* #### CLIENTS

  * ##### Client List
  * ##### Suspended Client
  * ##### Settings \[Segment,Billing Type,Credit Terms,Vertical,Client Type,SMS Template,Email \]Template\]

    ##### WORKER\]
* #### STAFFS

  * ##### Staff list
  * ##### Suspended Worker
  * ##### Settings \[Segment,Classification,Degree,Prefer Working Time,Transportation,Channel,Job Position,Worker Experience job Position,Bank,Operation State,SMS Template,Email Template\]
* #### SR

  * ##### SR List
  * ##### Timesheet
  * ##### Matching layer
  * ##### Settings \[SR Wage Type,SR wage Unit,Set Wage Attendance,Generate Attendance,Location Type, Referral Fee Amount,Bonus Settings, Report Staff Reasons,Cancel SR Reasons,Rejected Job Reasons,Attendance Reason,Notification Text,Contact Request type,Question Feedback,SR Template,\]
* ##### RECRUITMENT \(ONLY FOR OPS,CLIENT TEAM\)

  * ##### Leads List
  * ##### Leads Interview
* ##### PAYMENT \(REPLACEMENT FROM RECRUITMENT AND ONLY FOR FINANCE\)

  * ##### Payment Export
  * ##### Sales Order
  * ##### Settings \[Bank,Tax\]
* ##### PROFILE \[Preference, Invite Member include Role\]
* ##### LOGOUT

### 



