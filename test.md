### ADMIN DASHBOARD![](/assets/Admin Dashboard 2.0.png)

#### USER ACQUISITION![](/assets/Client Acquisition Flow 2.0.png)

* client registered by sales \[Big client &gt;1000 Mandays\],client profile made by sales team or client team.
* client that register itself \[organic client\] client portal will ask if more than &gt;1000 it will throw to sales if its &lt;1000 will ask for the credit card and if its verified we send email to activate the client portal account

### 

### FLOWCHART

#### Creating Client

![](/assets/Create Client @ 2.0.jpg)

Client Type Divided by 3 types if company register itself from website, as default we gonna create client profile as COMPANY\[CHILD\], if admin create they can choose 3 types :

* Group COMPANY \[PARENT\] means the biggest client for example \[JD/Ismaya\] Can see all information about all company and branch but CANNOT MAKE SR, for example group company can see all activity of the company or branch.

* COMPANY \[CHILD\] in here we can take example like \[PT ismaya F&B,JD warehouse\]  company can see activity for branch and company CAN MAKE SR for COMPANY itself or for BRANCH and CAN choose multiple location,and when the company to edit the SR for multiple location it can edit just 1 SR and it applied to all SR.

* BRANCH \[GRAND CHILD\] \[Sushi Groove Thamrin,Tokyo Belly GI\] Branch can create SR for ONLY that BRANCH and the location can will added automatically cannot choose other location.

Theres a state to register client \[LEADS,PENDING,ACTIVE\]

* LEADS if client register itself from client portal, the state become LEADS \[Client Name,PIC Name,PIC Number,Email,Vertical,Company Desc,Logo\] after they choose plan 0 &gt; 999 mandays and they input CC, after its Approved state will move to  PENDING on client profile.

* PENDING if client already input CC and its verified it become PENDING, on the system will send Email to CREATE PASSWORD.

* ACTIVE when client already create password for their login to client portal, they can MANAGE MEMBER to add more user so the other member of client can login to client portal, after 1st member of client add more member, system will send the email other member to activate their account by click the link on mail and CREATE PASSWORD.

#### Create Worker

#### ![](/assets/Create Worker @ 2.0.jpg)

Theres a State for the worker \[REGULAR,VIP\]

* LEADS if worker that register on the apps using WEB VIEW the state will be automatically be REGULAR, if they got invitation from Helpster for interview and if they pass the interview they will move to VIP, if they not pass the interview it need to keep as REGULAR for Matching layer and put on DB
* VERIFIED if worker that already pass the interview from Ops.

#### 

#### Create SR

![](/assets/SR Flow 2.0.jpg)

State for new SR \[Draft,Pending Approval,Posted,In Progress,Complete,Expired\]

* DRAFT if admin make SR but want to keep it later admin can keep it in DRAFT.
* PENDING APPROVAL if client make the SR then client submit it will automatically move to state PENDING APPROVAL. 
* POSTED when the admin make the SR at new dashboard when admin click SUBMIT it will move the state to POSTED. if the client hire the worker and they want to start immediately, they can start working at that time, no need to wait start time
* IN PROGRESS when the SR already pass the start date or worker already clock in it will move to the IN PROGRESS and other worker cannot apply in this state.

* COMPLETE When the SR already pass the end date it will move automatically to COMPLETE.

### STATE OF SR![](/assets/State of SR.jpg)

* Draft means when OPS create SR but they want to keep it Hold, they can put on Draft first.
* Pending Approval when CLIENT create SR from CLIENT PORTAL its automatically go to Pending Approval, waiting for ops approve their SR.
* Posted when the SR already approved from the OPS it will show SR on apps and worker can start apply
* In Progress when the worker start Clock in/out the state still at POSTED but on the dashboard will show on In Progress
* Complete when the SR pass the END DATE system will change the SR STATE to COMPLETE and the worker STATE also change to COMPLETE

### STATE OF WORKER THAT APPLY SR![](/assets/Worker State .jpg)

* Matched when worker got matched from matching layer it will show at the dashboard
* Applied when the worker applied it gonna show at the dashboard
* Contact when the OPS contact the worker, ops can detect which one already contact
* Interview Depends on SR if the SR need 2nd interview, worker STATE will move to INTERVIEW if its pass its gonna go to TRAINING \[Depends on SR settings\] or gonna go to HIRED, if worker not pass interview it will move state to CANCEL BY CLIENT/REJECT. 
* Training if the SR set to have training period,before they got hired they will go to Training period with different wage. 
* Hired when the worker pass the interview \[No Need training\] they will move to HIRED. 
* Confirmed when the worker accept the job from notification state will move to CONFIRMED.
* In Progress when the worker start CLOCK IN worker state will move to IN PROGRESS.
* Complete when the worker already pass the END DATE system automatically move the worker to COMPLETE state.

### 

### APPLICATION

![](/assets/Helpster 2.0 App Flowchart.png)

### NAVIGATION AT ADMIN DASHBOARD

* #### SUMMARY
* #### CLIENT

  * ##### Client List
  * ##### Suspended Client
  * ##### Settings \[Segment,Billing Type,Credit Terms,Vertical,Client Type,SMS Template,Email \]Template\]

    ##### WORKER\]
* #### WORKER

  * ##### Worker list
  * ##### Suspended Worker
  * ##### Settings \[Segment,Classification,Degree,Prefer Working Time,Transportation,Channel,Job Position,Worker Experience job Position,Bank,Operation State,SMS Template,Email Template\]
* #### SR

  * ##### SR List
  * ##### Timesheet
  * ##### Matching layer
  * ##### Settings \[SR Wage Type,SR wage Unit,Set Wage Attendance,Generate Attendance,Location Type, Referral Fee Amount,Bonus Settings, Report Staff Reasons,Cancel SR Reasons,Rejected Job Reasons,Attendance Reason,Notification Text,Contact Request type,Question Feedback,SR Template,\]
* ##### RECRUITMENT \(ONLY FOR OPS,CLIENT TEAM\)

  * ##### Leads List
  * ##### Leads Interview
* ##### PAYMENT \(REPLACEMENT FROM RECRUITMENT AND ONLY FOR FINANCE\)

  * ##### Payment Export
  * ##### Sales Order
  * ##### Settings \[Bank,Tax\]
* ##### PROFILE \[Preference, Invite Member include Role\]
* ##### LOGOUT



