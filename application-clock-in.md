CLOCK IN PAGE

![](/assets/Clockin1.png)

* Staff can tap I AM STARTING MY SHIFT then the button change to slider

![](/assets/clockin2.png)

* Staff can clock in by slide the &gt;&gt; 

![](/assets/clockin3.png)

![](/assets/clockin4.png)

* After done from the clock in it will have red alert say "WORKING" and the clock in time will record and the slider will change to clock out

CLOCK IN/OUT-  ERROR

![](/assets/Failed1.png)

* This error show when the Staff failed clock in/out 1st image show if they got bad internet connection and they force to try again, if they tap TRY AGAIN need to slide to clock in /out manually, if still cannot slide to clock in/out for 2nd time the 2nd image will show to contact our support team.

![](/assets/notinare.png)  

* This error will show if they clock in but not in area for clock in, if they tap CONTACT OUR TEAM it will to WA/LINE
* if Staff tap TRY AGAIN it will back to CLOCK IN page





