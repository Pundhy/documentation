NOTIFICATION 

![](/assets/Noti1.png)

* This Notification will show if they got hired from the client.
* if Staff tap the notification it will bring to CONGRATULATIONS page
* in this notification Staff can see Title of the notification, Client Name, and Position

![](/assets/Noti2.png)

* This Notification will show if they got Paid from Helpster.
* If Staff tap the notification it will bring the GOT PAID page
* in this notification Staff can see title of the notification and information to check their bank account.



