CLIENT LIST

In this page you can see all client list from LEADS,PENDING,ACTIVE and from here you can track which client that already ACTIVATE their account of still PENDING. if theres a lead client still dont wanna use our service, we can put to TRASH and if in 1 month they still dont want to use our service system will remove their account from our DB. If a client already put on trash and want to put back to lead can go to TRASH tab and click PUT BACK and it will move back to Leads Tab.

![](/assets/Client List.png)![](/assets/Client List 2.png)![](/assets/Client list 3.png)![](/assets/Client list 4.png)

CREATE CLIENT \[CHOOSE CLIENT TYPE\]  
After we click Create Client the pop up will show up and we need to choose client type GROUP COMPANY,COMPANY,BRANCH.![](/assets/Create client.png)

![](/assets/Create Client 2.png)![](/assets/Create Client 3.png)![](/assets/Create Client 4.png)

CREATE CLIENT \[GROUP COMPANY FORM\]

If Admin Click Group Company, this form will show .  
![](/assets/Group Company 1.png)![](/assets/Group Company 2.png)![](/assets/Group Company 3.png)![](/assets/Group Company 4.png)![](/assets/Group Company 5.png)![](assets/Group Company 6.png)

DETAIL CLIENT VIEW  
after fill the form and admin press CREATE, system will make client profile as leads, if its already fix want to use our service, admin can press SEND ACTIVATION and system will automatically move the state from LEADS to PENDING and system will send email to make password for the client.  
![](/assets/Group Company detail .png)![](/assets/Group Company detail 2.png)![](assets/Group Company detail 3.png)SEND NOTIFICATION \[SMS,EMAIL\]![](/assets/Group Company detail 4.png)![](/assets/Group Company detail 5.png)

GROUP COMPANY CHECK BRANCH  
this branch tab will only appear for GROUP COMPANY and COMPANY.  
![](/assets/Group Company detail 6.png)

GROUP COMPANY CHECK SR LIST  
in this tab its show how many posted COMPANY and BRANCH active, and GROUP COMPANY CANNOT make SR only COMPANY and BRANCH, COMPANY can make SR for BRANCH but BRANCH CANNOT make SR for COMPANY.  
![](/assets/Group Company detail 7.png)

GROUP COMPANY CHECK HISTORY LOG  
in this tab it will show Activity of Admin/Client from change name,Logo,Add SR,change detail of SR.  
![](/assets/Group Company detail 8.png)

CREATE CLIENT \[COMPANY FORM\]

In this page it will show if the Admin click on COMPANY, and this page  if they have Group Company need to add  @ GROUP COMPANY Name, if they dont have or just a company dont have branch, Group Company Name can leave it blank.![](/assets/Company 1.png)

### ![](/assets/Company 2.png)

### ![](/assets/Company 3.png)

CREATE CLIENT \[BRANCH FORM\]  
In this page it will show if the Admin click on BRANCH, and it need to put COMPANY name.  ![](/assets/Company 1.png)  
![](/assets/Branch 1.png)

### ![](/assets/Branch 2.png)

SUSPENDED CLIENT

![](/assets/Suspended Client.png)

### 



