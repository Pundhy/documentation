### CREATING NEW SR @ Odoo

Introducing New SR that combine between Shift and Dynamic, this SR will

* Allow Staff to clock in before start date.
* Deduct payment if they coming late.
* Set maximum 3 shift and no need to assign Staff for shift they are prefer.
* More simple for replace Staff if one of them not come, the Staff that already hired from client and manually assign for replace can just come and clock in.
* Ability to extend SR if some client want to extend the event or the job.
* auto cancel timesheet\[per day\] if the worker not clock in all day. 
* Company Can duplicate SR once they edit it can applied to all SR that duplicated \[TBC\]
* More payment if they working at public holiday \[TBC\].
* More payment if they working for replace someone shift \[TBC\].
* 1 SR can set more than 1 place to clock in/out \[TBC\].

### ![](/assets/New SR Odoo.png)

after we fill the information on CLIENT DETAIL & SR Information we move to SR Settings, at clock in/clock out we choose  
"NEW SR" and new menu will show DEDUCT PAYMENT means this feature allow you to deduct worker payment if they come in late \[CLOCK IN\].

For example we CHECKED this feature and we input clock in tolerance 30 mins

Wage 100.000 per day  
Shift 1 :  From 09.00 - 17.00  
Tolerance Clock in Before 30 mins After 30 mins  
Tolerance Clock out After 30 mins

the worker come at 09.32  
so worker late for 2 mins  
per mins is 208. 208x2 = 416 so the worker will deduct 416 \[100.000 - 416 = 99.584\]

### ![](/assets/New SR Odoo 2.png)

After done with SR settings we can move to SR Detail, in here we can activate Shift 2 and 3, and you can only custom start time and end time.  
tolerance before clock in per mins so its gonna detect the "STRANGE HOUR" its still allow worker to clock in earlier but gonna record on timesheet.

### ![](/assets/New SR Odoo 3.png)

in this Additional Information we need to make sure GENDER & AGE are correct because after we post the SR we cant edit anything other than "STAFF REQUIRED" and also this choice gonna be effect to the matching layer.

![](/assets/Extend SR.png)

on the New SR theres a function for EXTEND SR, if theres a client want to extend the SR and the state must be on \[POSTED\] and maximum to be extend SR  7 days before the END DATE. after we click on EXTEND SR pop up will show to choose START DATE - END DATE and also Days of week.

### Timesheet for NEW SR

### ![](/assets/Timesheet State.jpg)

* Schedule When the worker got hired from the client system will make schedule for them to clock in on that day.
* Clock in/Confirmed when the worker clock in system will recognize they at work and move the state to confirmed
* Cancel by worker if the worker not clock all day until next day system will cancel the timeheet for previous as we assume they not work on that day or  holiday.
* In Progress when the worker already confirmed the state will move to In progress.
* Pending Approval when the worker already clock out the system will move the state to pending approval.
* Approved when the client confirmed that worker come to work. 
* Dispute when the client not approved the worker,or the worker not going to work but clock in the area.

Export Timesheet theres 2 timesheet we can view for Export and Web View at client portal

WEB VIEW at client portal gonna look like this  
![](/assets/WebView Timesheet.png)

EXPORT timesheet from client portal gonna be like this, and we can choose want to export daily or weekly on the top is daily and on bottom is weekly.  
![](/assets/export timesheet.png)

