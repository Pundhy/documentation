# Summary

* [Helpster Guideline 2.0](README.md)
* [UI Guideline](ui-guideline.md)
* [Flowchart](test.md)
* [Create SR @ Odoo](create-sr-odoo.md)
* [APPLICATION - LOGIN](application-login.md)
* [APPLICATION - LOOKING FOR A JOB](application-looking-for-a-job.md)
* [APPLICATION-NOTIFICATION](application-notification.md)
* [APPLICATION-Got HIRED](application-got-hired.md)
* [APPLICATION - CLOCK IN](application-clock-in.md)
* [APPLICATION - TIMESHEET](application-timesheet.md)
* [APPLICATION-NOTIFICATION](application-notification.md)
* [DASHBOARD - NAVIGATION](dashboard-navigation.md)
* [DASHBOARD - LOGIN](dashboard-login.md)
* [DASHBOARD - CREATE STAFF](dashboard-create-staff.md)
* [DASHBOARD - CREATE CLIENT](dashboard-create-client.md)

