![](/assets/New Helpster Logo %28Low%29_Black.png)

### HELPSTER 2.0 GUIDELINE

Welcome to helpster guideline, in here you can learn how to use Helpster 2.0 on Admin Dashboard & application

### BUSINESS

### COUNTRY

* INDONESIA

* THAILAND

### ORGANIZATION CHART

### CLIENT LIST

* ISMAYA 
* NINJA EXPRESS
* LAKU 6
* JD

### APPLICATION GUIDELINE

* LOGIN
* LOOKING FOR A JOB
* NOTIFICATION
* GOT HIRED
* CLOCK IN/OUT
* TIMESHEET

### DASHBOARD GUIDELINE

* LOGIN
* STAFF
* CREATE CLIENT

### FAQ

* cannot clock in ![](assets/Clockinfaq1.png)  
  If its already correct need to check from worker side that working on that SR![](/assets/Clockinfaq2.png)if want to compare between work and current location more detail [https://www.movable-type.co.uk/scripts/latlong.html](https://www.movable-type.co.uk/scripts/latlong.html)

* not get notification : we can check if the notification already sent or not   
  if TH pne.helpster.co.th &gt; history log &gt;  status  
  if ID pne.helpster.id &gt; history log &gt;   status

* Create Timesheet : ![](/assets/Create_Timesheet.png)

* Cancel Timesheet : ![](assets/Cancel_Timesheet.png)
* Export Timesheet![](assets/ExportTimesheetfaq1.png)

* Edit SR : if the job already posted\(NEW SR Type\), Admin cannot edit the SR, the only admin can edit only the STAFF REQUIRED ![](/assets/StaffRequired.png)
* Change worker Phone Number![](/assets/Phonenumber1.png)After done editing the phone number, need to use debug mode to show "ACCESS Menu"  
  TH use admin.helpster.co.th/web?debug  
  ID use admin.helpster.id/web?debug  
  ![](/assets/phonenumber2.png)![](/assets/phonenumber3.png)

* u not authorize :if you got this error just follow this image below to fix it.

  * ![](/assets/image %284%29.png)

### 



