WHEN THE STAFF GOT SELECTED BY THE STAFF \(CONFIRMATION PAGE\)

![](/assets/Hired1.png)

* Staff will see the celebration layout that got hired from that job
* Staff can see the information Start date - End date and Start - End time job
* Staff can see the information about the location from home to the job.

![](/assets/Hired2.png)

* Staff can see information about how much they will earn per Daily/Weekly/Monthly.
* Staff can see information about the Start - End Date and when start per shift.

![](/assets/Hired4.png)

* Staff can see information about detail task and requirement for the job.
* Staff can see easily contact Helpster Support if something wrong.

![](/assets/Hired3.png)

* Staff can do I'M NOT AVAILABLE &  I AM READY, If the staff choose IM NOT AVAILABLE they gonna cancel the job and the POP UP reason will show, If the staff choose I AM READY they accept the job and the CONGRATULATIONS PAGE will show.

CONGRATULATIONS PAGE

![](/assets/Accept1.png)

* Staff can see the popup congrats that has been hired.
* Staff can click button thank you and it will close the popup.
* Staff can ONLY see ACTIVE JOB and cannot see other job before complete.

![](assets/JobDetailcancel.png)

* Staff will see this image if they press IM NOT AVAILABLE



WHEN THE STAFF CLICK JOB DETAIL WHEN THEY GOT HIRED

![](assets/Working1.png)

![](/assets/Working2.png)

![](/assets/Working3.png)

![](/assets/Working4.png)

