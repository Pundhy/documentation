WHEN STAFF LOGIN AND DONT HAVE JOB.  
will open NEW tab, its show that matched from staff profile

![](assets/Test.png)  
 WHEN STAFF OPEN APP BUT ALREADY APPLIED JOBS

![](/assets/JobListApplied.png)

WHEN STAFF OPEN APPS JOB LIST EMPTY

![](/assets/JobListEmpty.png)

JOB DETAIL

![](assets/JobDetail1.png)

* Staff can see client logo
* Staff can see job position, job title
* staff can see location of the map

![](/assets/JobDetail2.png)

* Staff can see information will earn in Daily/Weekly/Monthly
* Staff can see information of Start date - End date and start time for each shift

![](/assets/JobDetail3.png)

* Staff can see information detail task and requirement for the job

![](/assets/JobDetail4.png)

* Staff can easily contact Helpster team \(TH with Line, ID with Whatsapp\) if theres something wrong with the job.
* Staff can easily see this job no need 2nd interview from the client,if theres 2nd interview the image below will show![](/assets/JobDetailInterview.png) 

When the staff already apply the job the button for apply change to WITHDRAW & CHAT NOW like image below

![](/assets/JobDetailApplied.png)

If theres Staff that want to withdraw or cancel the job then they hit WITHDRAW its gonna show this image below

![](/assets/JobDetailcancel.png)

if staff already choose one and hit OK,this popup will show, if hit Message now its gonna launch \(ID = Whatsapp, TH=LINE\)

![](/assets/JobDetailCancel2.png)

